mport random

def seleccionar_palabra():
    palabras = ["python", "programacion", "ahorcado", "juego", "computadora", "desarrollador", "aprendizaje"]
    return random.choice(palabras)

def jugar_ahorcado():
    palabra = seleccionar_palabra()
    palabra_adivinada = "_" * len(palabra)
    intentos = 6
    letras_usadas = []
    
    print("¡Bienvenido al juego del ahorcado!")
    print(palabra_adivinada)
    
    while "_" in palabra_adivinada and intentos > 0:
        letra = input("Ingresa una letra: ").lower()
        
        if letra in letras_usadas:
            print("Ya has usado esa letra. Intenta con otra.")
            continue
        
        letras_usadas.append(letra)
        
        if letra in palabra:
            print("¡Correcto!")
            nueva_palabra = ""
            for i in range(len(palabra)):
                if letra == palabra[i]:
                    nueva_palabra += letra
                else:
                    nueva_palabra += palabra_adivinada[i]
            palabra_adivinada = nueva_palabra
        else:
            print("Incorrecto. Pierdes un intento.")
            intentos -= 1
        
        print("Palabra actual: " + palabra_adivinada)
        print("Letras usadas: " + ", ".join(letras_usadas))
        print("Intentos restantes: " + str(intentos))
    
    if "_" not in palabra_adivinada:
        print("¡Felicidades! Has adivinado la palabra: " + palabra)
    else:
        print("Has perdido. La palabra era: " + palabra)

jugar_ahorcado()